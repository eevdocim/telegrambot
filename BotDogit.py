#t.me/dogit_bot
import os
import sys
import re
import configparser
import requests
import json
from urllib.request import urlretrieve
import telebot
from telebot import types

def resource_path(relative_path):
    try:
        base_path = sys._MEIPASS
    except Exception:
        base_path = os.path.abspath(".")
    return os.path.join(base_path, relative_path)

config = configparser.ConfigParser()
config.read(resource_path('config.ini'))

def CheckKey():
    global config
    if 'BOT' in config:
        if not 'key' in config['BOT']:
            return False
        else:
            return True
    else:
        return False

if not CheckKey():
    config.add_section('BOT')
    config.set('BOT', 'key', '')

superuser = False
bot = telebot.TeleBot(config["BOT"]["key"])

def Test(message):
    print("Function test")
    bot.send_message(message.chat.id, 'Работает из теста')
    bot.send_message(message.chat.id, 'Удаление клавиатуры /rm')
    start = types.ReplyKeyboardMarkup(True, False)
    start.row('Как дела ?')
    start.row('Привет')
    start.row('Как погода ?')
    start.row('Контакты')
    bot.send_message(message.from_user.id, 'Выбери вопрос', reply_markup=start)

def Help(message):
    bot.send_message(
        message.from_user.id,
        "Напиши: \nПривет \nIP \nКомманды: \n/help - Помощь\n/start - Начало работы\n/save - Запомнить"
    )

def Save(message):
    bot.send_message(
        message.from_user.id,
        "Запомнил"
    )

@bot.message_handler(commands=['start'])
def start_message(message):
    bot.send_message(message.chat.id, 'Работаю')

@bot.message_handler(commands=['help'])
def start_message(message):
    Help(message)

@bot.message_handler(commands=['save'])
def start_message(message):
    Save(message)

@bot.message_handler(commands=['test'])
def start_message(message):
    Test(message)

@bot.message_handler(commands=['rm'])
def start_message(message):
    bot.send_message(message.from_user.id, 'Клавиатура удалена', reply_markup=types.ReplyKeyboardRemove())


@bot.channel_post_handler(content_types=['text'])
def get_text_messages_channel(message):
    bot.send_message(message.chat.id, "Привет")

@bot.message_handler(content_types=['text'])
def get_text_messages_chat(message):
    if message.from_user.username in config["USER"]["admins"]:
        superuser = True
    if message.text.lower() == "ip" and superuser:
        bot.send_message(message.from_user.id, requests.get('https://ramziv.com/ip').text)
    elif message.text.lower() == "привет":
        bot.send_message(message.from_user.id, "Привет")
    elif re.sub('\W+','',  message.text.lower()) == "какдела":
        bot.send_message(message.from_user.id, "Хорошо")
    else:
        keyboard = types.InlineKeyboardMarkup()
        key_yes = types.InlineKeyboardButton(text='Да', callback_data='yes')
        keyboard.add(key_yes)
        key_no= types.InlineKeyboardButton(text='Нет', callback_data='no')
        keyboard.add(key_no)
        bot.send_message(message.from_user.id, text='Я тебя не понимаю. Помочь ?', reply_markup=keyboard)

@bot.callback_query_handler(func=lambda call: True)
def callback_worker(call):
    if call.data == "yes":
        Help(call.message)
    elif call.data == "no":
        bot.send_message(call.message.chat.id, 'Хорошо :)')
    bot.delete_message(call.message.chat.id, call.message.message_id)

print('Start BOT DOGIT')

bot.infinity_polling(
    timeout=int(config["SETTINGS"]["timeout"]), 
    interval=int(config["SETTINGS"]["interval"])
)
